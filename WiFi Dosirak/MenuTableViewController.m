//
//  MenuTableViewController.m
//  WiFi Dosirak
//
//  Created by Alan.yu on 2016. 10. 1..
//  Copyright © 2016년 Alan.yu. All rights reserved.
//

#import "MenuTableViewController.h"
#import "AppDelegate.h"
#import "FrontViewController.h"
#import "SWRevealViewController.h"

@interface MenuTableViewController () {
    NSArray<NSDictionary<NSString *, NSString *> *> *_urls;
}
@end

@implementation MenuTableViewController

- (void)viewWillAppear:(BOOL)animated {
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _urls = app.menuItems;
    
    [super viewWillAppear:animated];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _urls.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSInteger row = indexPath.row;
    
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    NSDictionary *item = [_urls objectAtIndex:row];
    cell.textLabel.text = [item objectForKey:@"title"];
    
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [sender isKindOfClass:[UITableViewCell class]] )
    {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        NSInteger row = indexPath.row;
        
        NSDictionary<NSString *, NSString *> *item = [_urls objectAtIndex:row];
        NSURL *url = [NSURL URLWithString:[item objectForKey:@"url"]];

        UINavigationController *navController = segue.destinationViewController;
        FrontViewController *cvc = [navController childViewControllers].firstObject;
        
        if ( [cvc isKindOfClass:[FrontViewController class]] )
        {
            [cvc setURL:url];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if (revealViewController != nil) {
        
        NSDictionary<NSString *, NSString *> *item = [_urls objectAtIndex:indexPath.row];
        NSURL *url = [NSURL URLWithString:[item objectForKey:@"url"]];
        
        [revealViewController performSelector:@selector(rightRevealToggle:) withObject:self];
        
        UINavigationController *navController = (UINavigationController *)revealViewController.frontViewController;
        FrontViewController *cvc = [navController childViewControllers].firstObject;
        
        if ( [cvc isKindOfClass:[FrontViewController class]] )
        {
            [cvc setURL:url];
        }
    }
}

@end
