//
//  FrontViewController.h
//  WiFi Dosirak
//
//  Created by Alan.yu on 2016. 10. 1..
//  Copyright © 2016년 Alan.yu. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface FrontViewController : UIViewController

- (void)setURL: (NSURL *) url;

@end
