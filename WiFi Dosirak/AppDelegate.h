//
//  AppDelegate.h
//  WiFi Dosirak
//
//  Created by Alan.yu on 2016. 10. 1..
//  Copyright © 2016년 Alan.yu. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWRevealViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) SWRevealViewController *viewController;

- (NSString *)homeUrl;
- (NSArray *)menuItems;

@end

