//
//  AppDelegate.m
//  WiFi Dosirak
//
//  Created by Alan.yu on 2016. 10. 1..
//  Copyright © 2016년 Alan.yu. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate()
- (void)loadMenuItems;
@end

@implementation AppDelegate

NSDictionary<NSString *, NSDictionary *> *_menuItems;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self loadMenuItems];
    
    [NSThread sleepForTimeInterval:1];
    
    return YES;
}

- (NSString *)homeUrl {
    NSDictionary *homeItem = [_menuItems objectForKey:@"home"];
    NSString *homeUrl = [homeItem objectForKey:@"url"];
    
    return homeUrl;
}

- (NSArray *)menuItems {
    NSMutableArray *menuArray =[[NSMutableArray alloc] init];
    
    [menuArray addObject:(NSDictionary *)[_menuItems objectForKey:@"home"]];
    [menuArray addObject:(NSDictionary *)[_menuItems objectForKey:@"reserve"]];
    
    NSArray *result = [NSArray arrayWithArray:menuArray];
    
    return result;
}

- (BOOL)isLogin {
    NSArray<NSHTTPCookie *> *availableCookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:[self homeUrl]]];
    
#ifdef DEBUG
    for(NSHTTPCookie *cookie in availableCookies) {
        NSLog(@"%@ = %@", cookie.name, cookie.value);
    }
#endif
    
    return YES;
}

- (void)loadMenuItems {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Setting" ofType:@"plist"];
    NSDictionary *items = [NSDictionary dictionaryWithContentsOfFile: path];

#ifdef DEBUG
    for (NSDictionary *item in items) {
        NSLog(@"%@", item);
    }
#endif
    
    _menuItems = [items copy];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    NSData *cookieData = [NSKeyedArchiver archivedDataWithRootObject:cookies];
    [[NSUserDefaults standardUserDefaults] setObject:cookieData forKey:@"Cookies"];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    NSData *cookiesData = [[NSUserDefaults standardUserDefaults] objectForKey:@"Cookies"];
    if ( [cookiesData length] )
    {
        NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData:cookiesData];
        for ( NSHTTPCookie *cookie in cookies )
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}



- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
