//
//  FrontViewController.m
//  WiFi Dosirak
//
//  Created by Alan.yu on 2016. 10. 1..
//  Copyright © 2016년 Alan.yu. All rights reserved.
//

#import "FrontViewController.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"

@interface FrontViewController()

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic) IBOutlet UIBarButtonItem* menuItem;

@end

@implementation FrontViewController

#pragma mark - View lifecycle


- (void)viewDidLoad
{
	[super viewDidLoad];

    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController ) {
        self.revealViewController.rightViewRevealOverdraw = 0.0f;

        [self.menuItem setTarget: self.revealViewController];
        [self.menuItem setAction: @selector( rightRevealToggle: )];
        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
    
    NSString *homeUrl = [(AppDelegate *)[[UIApplication sharedApplication] delegate] homeUrl];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:homeUrl]];
    
    [_webView loadRequest:request];
    
}

- (void)setURL: (NSURL *) url {
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
}

@end
